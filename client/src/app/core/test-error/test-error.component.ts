import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MyError } from 'src/app/shared/models/my-httpErrorResponse';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-test-error',
  templateUrl: './test-error.component.html',
  styleUrls: ['./test-error.component.scss'],
})
export class TestErrorComponent implements OnInit {
  baseUrl = environment.apiUrl;
  validationErros: string[] = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {}

  get404Error() {
    this.http.get(this.baseUrl + 'products/42').subscribe(
      (resp) => {
        console.log('response: ', resp);
      },
      (err) => {
        console.log('error: ', err);
      },
    );
  }

  get500Error() {
    this.http.get(this.baseUrl + 'buggy/servererror').subscribe(
      (resp) => {
        console.log('response: ', resp);
      },
      (err) => {
        console.log('error: ', err);
      },
    );
  }

  get400Error() {
    this.http.get(this.baseUrl + 'buggy/badrequest').subscribe(
      (resp) => {
        console.log('response: ', resp);
      },
      (err) => {
        console.log('error: ', err);
      },
    );
  }

  get400ValidationError() {
    this.http.get(this.baseUrl + 'products/fortytwo').subscribe(
      (resp) => {
        console.log('response: ', resp);
      },
      (err: MyError) => {
        console.log('error: ', err);
        this.validationErros = err.errors;
      },
    );
  }
}

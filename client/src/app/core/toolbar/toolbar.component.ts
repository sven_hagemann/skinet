import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountService } from 'src/app/account/account.service';
import { BasketService } from 'src/app/basket/basket.service';
import { IBasket } from 'src/app/shared/models/basket';
import { IUser } from 'src/app/shared/models/user';
import { MenuItem } from '../../menu-item';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  basket$: Observable<IBasket>;
  currUser$: Observable<IUser>;

  menuItemsMiddle: MenuItem[] = [
    {
      label: 'Home',
      // icon: 'help',
      path: '/',
      showOnMobile: false,
      showOnTablet: true,
      showOnDesktop: true,
    },
    {
      label: 'Shop',
      // icon: 'notes',
      path: '/shop',
      showOnMobile: false,
      showOnTablet: true,
      showOnDesktop: true,
    },
    {
      label: 'Errors',
      // icon: 'slideshow',
      path: '/test-error',
      showOnMobile: false,
      showOnTablet: false,
      showOnDesktop: true,
    },
    // {
    //   label: 'Contact',
    //   // icon: 'slideshow',
    //   path: '/contact',
    //   showOnMobile: false,
    //   showOnTablet: false,
    //   showOnDesktop: true
    // },
  ];

  menuItemsRight: MenuItem[] = [
    {
      label: 'Login',
      // icon: 'rss_feed',
      path: 'account/login',
      showOnMobile: true,
      showOnTablet: true,
      showOnDesktop: true,
    },
    {
      label: 'Sign up',
      // icon: 'login',
      path: 'account/register',
      showOnMobile: true,
      showOnTablet: true,
      showOnDesktop: true,
    },
  ];

  constructor(private basketService: BasketService, private accountService: AccountService) {}

  ngOnInit(): void {
    this.basket$ = this.basketService.basket$;
    this.currUser$ = this.accountService.currUser$;
  }

  logout(){
    this.accountService.logout();
  }
}

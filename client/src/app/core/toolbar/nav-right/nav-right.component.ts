import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MenuItem } from 'src/app/menu-item';
import { IUser } from 'src/app/shared/models/user';

@Component({
  selector: 'app-nav-right',
  templateUrl: './nav-right.component.html',
  styleUrls: ['./nav-right.component.scss']
})
export class NavRightComponent implements OnInit {

  @Input()
  menuItemsRight: MenuItem[];

  @Input() user: IUser;

  @Output() logout = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}

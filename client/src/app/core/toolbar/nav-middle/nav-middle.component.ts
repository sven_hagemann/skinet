import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from 'src/app/menu-item';

@Component({
  selector: 'app-nav-middle',
  templateUrl: './nav-middle.component.html',
  styleUrls: ['./nav-middle.component.scss']
})
export class NavMiddleComponent implements OnInit {

  @Input()
  menuItemsMiddle: MenuItem[];

  constructor() { }

  ngOnInit(): void {
  }

}

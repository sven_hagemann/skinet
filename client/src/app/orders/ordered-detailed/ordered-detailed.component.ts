import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IOrder } from 'src/app/shared/models/order';
import { OrdersService } from '../orders.service';

@Component({
  selector: 'app-ordered-detailed',
  templateUrl: './ordered-detailed.component.html',
  styleUrls: ['./ordered-detailed.component.scss'],
})
export class OrderedDetailedComponent implements OnInit {
  order: IOrder;
  displayedColumns: string[] = ['product', 'price', 'quantity', 'total'];

  constructor(
    private route: ActivatedRoute,
    private ordersService: OrdersService,
  ) {}

  ngOnInit(): void {
    this.ordersService
      .getOrderDetaied(+this.route.snapshot.paramMap.get('id'))
      .subscribe(
        (order: IOrder) => {
          this.order = order;
        },
        (err) => {
          console.log('Error: ', err);
        },
      );
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersComponent } from './orders.component';
import { OrderedDetailedComponent } from './ordered-detailed/ordered-detailed.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'', component: OrdersComponent},
  {path:':id', component: OrderedDetailedComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }

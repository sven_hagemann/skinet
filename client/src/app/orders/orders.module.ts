import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersComponent } from './orders.component';
import { OrderedDetailedComponent } from './ordered-detailed/ordered-detailed.component';
import { OrdersRoutingModule } from './orders-routing.module';
import { CoreModule } from '../core/core.module';



@NgModule({
  declarations: [
    OrdersComponent,
    OrderedDetailedComponent
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    CoreModule
  ]
})
export class OrdersModule { }

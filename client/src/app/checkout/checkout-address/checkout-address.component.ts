import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AccountService } from 'src/app/account/account.service';

@Component({
  selector: 'app-checkout-address',
  templateUrl: './checkout-address.component.html',
  styleUrls: ['./checkout-address.component.scss'],
})
export class CheckoutAddressComponent implements OnInit {
  @Input() checkoutForm: FormGroup;
  // errors: string[];

  constructor(
    private accountService: AccountService,
    private toastr: ToastrService,
  ) {}

  ngOnInit(): void {}

  saveUserAddress() {
    this.accountService
      .updateUserAddress(this.checkoutForm.get('addressForm').value)
      .subscribe(
        () => {
          this.toastr.success('Address saved');
        },
        (err) => {
          this.toastr.error(err.message);
          console.log('Error: ', err);          
        },
      );
  }

  // onSubmit() {
  //   this.accountService.register(this.registerForm.value).subscribe(
  //     (resp) => {
  //       this.router.navigateByUrl('/shop');
  //     },
  //     (err) => {
  //       console.log('Error: ', err);
  //       this.errors = err.errors;
  //     },
  //   );
  // }
}

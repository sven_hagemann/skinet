import { Component, OnInit } from '@angular/core';
import { AccountService } from './account/account.service';
import { BasketService } from './basket/basket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'Skinet';

  constructor(
    private basketService: BasketService,
    private accountService: AccountService,
  ) {}

  ngOnInit(): void {
    this.loadBasket();
    this.loadCurrUser();
  }
  loadCurrUser() {
    const token = localStorage.getItem('token');

    this.accountService.loadCurrUser(token).subscribe(
      () => {
        console.log('loaded user');
      },
      (err) => {
        console.log('Error: ', err);
      },
    );
  }

  loadBasket() {
    const basketId = localStorage.getItem('basket_id');
    if (basketId) {
      this.basketService.getBasket(basketId).subscribe(
        () => {
          console.log('initialised basket');
        },
        (err) => {
          console.log('error: ', err);
        },
      );
    }
  }
}

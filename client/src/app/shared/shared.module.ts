import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material/paginator';
import { PagerComponent } from './components/pager/pager.component';
import { OrderTotalsComponent } from './components/order-totals/order-totals.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { StepperComponent } from './components/stepper/stepper.component';
import { BasketSummaryComponent } from './components/basket-summary/basket-summary.component';

const sharedModule = [ReactiveFormsModule, MatPaginatorModule, CdkStepperModule];

@NgModule({
  declarations: [PagerComponent, OrderTotalsComponent, StepperComponent, BasketSummaryComponent],
  imports: [CommonModule, sharedModule, CoreModule],
  exports: [
    PagerComponent,
    OrderTotalsComponent,
    StepperComponent,
    BasketSummaryComponent,
    sharedModule,
  ],
})
export class SharedModule {}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.scss'],
})
export class PagerComponent implements OnInit {
  @Input() totalCount: number;

  @Input() pageIndex: number;

  @Input() pageSize: number;

  @Input() pageSizeOptions: number[];

  @Input() showFirstLastButtons: boolean;

  @Output() pageChanged = new EventEmitter<PageEvent>();

  constructor() {}

  ngOnInit(): void {}

  onPagerChanded(evt: PageEvent) {
    this.pageChanged.emit(evt);
  }
}

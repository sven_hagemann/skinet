export class ShopParams {
  brandId = 0;
  typeId = 0;
  sort = 'name';
  pageIndex = 0;
  pageSize = 5;
  search: string;
}

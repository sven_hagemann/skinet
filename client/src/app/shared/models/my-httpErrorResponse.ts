import { HttpErrorResponse } from '@angular/common/http';

export class MyHttpErrorResponse extends HttpErrorResponse {
  error: MyError;
}

export interface MyError {
  message: string;
  statusCode: number;
  errors?: string[];
  details?: string;
}

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  // Route zum Shop
  // oder wenn wir vom Auth-Guard kommen (wegen fehlenden Logins) die Ziel-Route zur Page, welche abgewiesen wurde
  returnUrl: string;

  constructor(
    private accountService: AccountService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.returnUrl =
    // Wenn sich der Query:"...login?returnUrl=..." in der Url befindet, dann wurde vom Auth-Guard die Ziel-Route verweigert und hierher geleitet
    // im UrlQuery "returnUrl" steht der Path zur Page, welcher abgelehnt wurde (weil noch nicht eingeloggt)
    // zu dieser Page wird navigiert, sobald der Login erfolgreich ist. Also zu "/shop" oder zur vom Auth-Gard abgewiesenen Page
      this.activatedRoute.snapshot.queryParams.returnUrl || '/shop'; 
    this.createLoginForm();
  }

  createLoginForm() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$'),
      ]),
      password: new FormControl('', Validators.required),
    });
  }

  onSubmit() {
    // console.log('LoginForm Values: ', this.loginForm.value);
    this.accountService.login(this.loginForm.value).subscribe(
      () => {
        this.router.navigateByUrl(this.returnUrl);
      },
      (err) => {
        console.log('Error: ', err);
      },
    );
  }
}

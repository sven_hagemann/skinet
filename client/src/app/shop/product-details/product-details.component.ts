import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BasketService } from 'src/app/basket/basket.service';
import { IProduct } from 'src/app/shared/models/product';
import { ShopService } from '../shop.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent implements OnInit {
  product: IProduct;
  quantity = 1;

  constructor(
    private readonly shopService: ShopService,
    private readonly activateRoute: ActivatedRoute,
    private readonly basketService: BasketService
    ) {}

  ngOnInit(): void {
    this.loadProduct();
  }

  loadProduct() {
    this.shopService.getProduct(+this.activateRoute.snapshot.paramMap.get('id')).subscribe(
      (product) => {
        this.product = product;
      },
      (error) => {
        console.log('error: ', error);
      },
    );
  }

  addItemToBasket(){
    this.basketService.addItemToBasket(this.product, this.quantity);
  }

  incremmentQuantity(){
    this.quantity++;
  }

  decremmentQuantity(){
    if (this.quantity > 1) {
      this.quantity--;
    }
  }
}

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { IBrand } from '../shared/models/brand';
import { IProduct } from '../shared/models/product';
import { IType } from '../shared/models/product-type';
import { ShopParams } from '../shared/models/shopParams';
import { ShopService } from './shop.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],
})
export class ShopComponent implements OnInit {

  // static: true => wenn das Child-Elmenet (hier die Textbox) statisch ist
  //  static heißt das Child-Element ist im DOM immer vorhanden und wir nicht dynamisch entfernt (z.B. durch *ngIf)
  @ViewChild('search', { static: true }) searchTerm: ElementRef;

  currSelected = '';
  products: IProduct[];
  brands: IBrand[];
  types: IType[];
  shopParams: ShopParams;
  sortOptions = [
    { name: 'Alphabetical', value: 'name' },
    { name: 'Price: Low to High', value: 'priceAsc' },
    { name: 'Price: High to Low', value: 'priceDesc' },
  ];

  length: number;
  pageSizeOptions = [3, 5, 10, 25];
  showFirstLastButtons = true;

  constructor(public readonly shopService: ShopService) {
    this.shopParams = this.shopService.getShopParams();
  }

  ngOnInit(): void {
    this.getProducts(true);
    this.getTypes();
    this.getBrands();
  }

  getProducts(useCache = false) {
    this.shopService.getProducts(useCache).subscribe(
      (response) => {
        console.log('response: ', response);

        this.products = response.data;
        this.length = response.count;
      },
      (error) => {
        console.log('error: ', error);
      },
    );
  }

  getBrands() {
    this.shopService.getBrands().subscribe(
      (response) => {
        this.brands = [{ id: 0, name: 'All' }, ...response]; // dem Array ein Item am Anfang hinzufügen
      },
      (error) => {
        console.log('error: ', error);
      },
    );
  }

  getTypes() {
    this.shopService.getTypes().subscribe(
      (response) => {
        this.types = [{ id: 0, name: 'All' }, ...response];
      },
      (error) => {
        console.log('error: ', error);
      },
    );
  }

  brandClicked(brandId: number) {
    const params = this.shopService.getShopParams();
    params.brandId = brandId;
    params.pageIndex = 1;
    this.shopService.setShopParams(params);
    this.getProducts();
  }

  typeClicked(typeId: number) {
    const params = this.shopService.getShopParams();
    params.typeId = typeId;
    params.pageIndex = 1;
    this.shopService.setShopParams(params);
    this.getProducts();
  }

  onSortSelected(sort: string) {
    const params = this.shopService.getShopParams();
    params.sort = sort;
    this.shopService.setShopParams(params);
    this.getProducts();
  }

  handlePagingEvent(event: PageEvent) {
    const params = this.shopService.getShopParams();
    this.length = event.length;
    params.pageSize = event.pageSize;
    params.pageIndex = event.pageIndex;
    this.shopService.setShopParams(params);

    this.getProducts(true);
  }

  onSearch(){
    const params = this.shopService.getShopParams();
    params.search = this.searchTerm.nativeElement.value;
    params.pageIndex = 1;
    this.shopService.setShopParams(params);
    this.getProducts();
  }

  onReset(){
    this.searchTerm.nativeElement.value = '';
    this.shopParams = new ShopParams();
    this.shopService.setShopParams(this.shopParams); // Reset all params
    this.getProducts();
  }
}

import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { IBrand } from '../shared/models/brand';
import { IPagination, Pagination } from '../shared/models/pagination';
import { IProduct } from '../shared/models/product';
import { IType } from '../shared/models/product-type';
import { ShopParams } from '../shared/models/shopParams';

@Injectable({
  providedIn: 'root',
})
export class ShopService {
  baseUrl = 'https://localhost:5001/api/';
  products: IProduct[] = [];
  brands: IBrand[] = [];
  types: IType[] = [];
  pagination = new Pagination();
  shopParams = new ShopParams();
  productCache = new Map();

  constructor(private http: HttpClient) {}

  // tslint:disable-next-line: typedef
  getProducts(useCache: boolean) {
    if (useCache === false) {
      this.productCache = new Map(); // Cache resetten
    }

    if (this.productCache.size > 0 && useCache === true) {
      const cacheKey = Object.values(this.shopParams).join('-');
      if (this.productCache.has(cacheKey)) {
        this.pagination.data = this.productCache.get(cacheKey);
        return of(this.pagination);
      }
    }

    console.log('brandId: ', this.shopParams.brandId);
    console.log('typeId: ', this.shopParams.typeId);

    let params = new HttpParams();

    if (this.shopParams.brandId !== 0) {
      params = params.append('brandId', this.shopParams.brandId.toString());
    }

    if (this.shopParams.typeId !== 0) {
      params = params.append('typeId', this.shopParams.typeId.toString());
    }

    if (this.shopParams.search) {
      params = params.append('search', this.shopParams.search);
    }

    params = params.append('sort', this.shopParams.sort);
    params = params.append('pageIndex', this.shopParams.pageIndex.toString());
    params = params.append('pageSize', this.shopParams.pageSize.toString());

    return this.http
      .get<IPagination>(this.baseUrl + 'products', {
        observe: 'response',
        params,
      })
      .pipe(
        map((resp) => {
          const cacheKey = Object.values(this.shopParams).join('-');
          this.productCache.set(cacheKey, resp.body.data);
          // this.products = [...this.products, ...resp.body.data]; // dem Cache hinzufügen
          this.pagination = resp.body;
          return this.pagination;
        }),
      );
  }

  setShopParams(params: ShopParams) {
    this.shopParams = params;
  }

  getShopParams() {
    return this.shopParams;
  }

  getProduct(id: number) {
    let product: IProduct;

    this.productCache.forEach((products: IProduct[]) => {
      product = products.find((p) => p.id === id);
    });

    if (product) {
      return of(product);
    }

    return this.http.get<IProduct>(this.baseUrl + 'products/' + id);
  }

  getBrands(): Observable<IBrand[]> {
    if (this.brands.length > 0) {
      return of(this.brands);
    }
    return this.http.get<IBrand[]>(this.baseUrl + 'products/brands').pipe(
      tap((resp) => {
        this.brands = resp;
      }),
    );
  }

  getTypes(): Observable<IType[]> {
    if (this.types.length > 0) {
      return of(this.types);
    }

    let arr: string[];

    return this.http.get<IType[]>(this.baseUrl + 'products/types').pipe(
      tap((resp) => {
        this.types = resp;
      }),
    );
  }
}

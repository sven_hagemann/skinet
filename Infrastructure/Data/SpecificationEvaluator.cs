using System.Linq;
using Core.Entities;
using Core.Specifications;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class SpecificationEvaluator<TEntity> where TEntity : BaseEntity
    {
        public static IQueryable<TEntity> GetQuery(IQueryable<TEntity> inputQuery, ISpecification<TEntity> spec)
        {
            var query = inputQuery;

            if (spec.Criteria != null)
            {
                // query => z.B. Product-Entity (DbSet) as Queryable
                // spec.Criteria => z.B. p => p.ProductId == id
                query = query.Where(spec.Criteria);
            }

            if (spec.OrderBy != null)
            {
                // query => z.B. Product-Entity as Queryable
                // spec.Criteria => z.B. p => p.ProductId == id
                query = query.OrderBy(spec.OrderBy);
            }

            if (spec.OrderByDescending != null)
            {
                // query => z.B. Product-Entity as Queryable
                // spec.Criteria => z.B. p => p.ProductId == id
                query = query.OrderByDescending(spec.OrderByDescending);
            }

            // Fügt assoziierte Tabellen hinzu => eager loading, wenn vorhanden
            query = spec.Includes.Aggregate(query, (current, include) => current.Include(include));

            // Immer nach dem Filtern der Daten ausführen
            // Immer nach dem Sortieren der Daten ausführen
            if (spec.IsPagingEnabled)
            {
                query = query.Skip(spec.Skip).Take(spec.Take);
            }

            return query;
        }
    }
}
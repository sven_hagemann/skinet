using Microsoft.EntityFrameworkCore;
using Core.Entities;
using System.Reflection;
using System.Linq;
using Core.Entities.OrderAggregate;
using System;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Infrastructure.Data
{
    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductBrand> ProductBrands { get; set; } // Fremdschlüssel in der Product-Tabelle
        public DbSet<ProductType> ProductTypes { get; set; } // Fremdschlüssel in der Product-Tabelle
        public DbSet<Order> Orders { get; set; } // Fremdschlüssel in der Product-Tabelle
        public DbSet<OrderItem> OrderItems { get; set; } // Fremdschlüssel in der Product-Tabelle
        public DbSet<DeliveryMethod> DeliveryMethods { get; set; } // Fremdschlüssel in der Product-Tabelle

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            // Sqlite-DB kennt nicht den Datentyp "decimal" 
            if (Database.ProviderName == "Microsoft.EntityFrameworkCore.Sqlite")
            {
                foreach (var entityType in modelBuilder.Model.GetEntityTypes()) // entityType => z.B. Product oder Fan
                {
                    // hole alle Eigenschaften vom Typ "decimal" aus der akt. Entity und ändere in "double"
                    var decimalProps = entityType.ClrType.GetProperties().Where(p => p.PropertyType == typeof(decimal));

                    foreach (var decProp in decimalProps)
                    {
                        modelBuilder.Entity(entityType.Name).Property(decProp.Name).HasConversion<double>();
                    }

                    // hole alle Eigenschaften vom Typ "DateTimeOffset" aus der akt. Entity und ändere in "double"
                    var dateTimeProps = entityType.ClrType.GetProperties().Where(p => p.PropertyType == typeof(DateTimeOffset));

                    foreach (var dateProp in dateTimeProps)
                    {
                        modelBuilder.Entity(entityType.Name).Property(dateProp.Name).HasConversion( new DateTimeOffsetToBinaryConverter());
                    }
                }
            }
        }
    }
}
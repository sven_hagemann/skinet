using System;
using Core.Entities.OrderAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Data.Config
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.OwnsOne(order => order.ShipToAddress, addr =>
            {
                addr.WithOwner();
            });

            // OrderStatus in String umwandeln
            builder.Property(order => order.Status).HasConversion(
                status => status.ToString(),
                status => (OrderStatus)Enum.Parse(typeof(OrderStatus), status)
            );

            builder.HasMany(order=>order.OrderItems).WithOne().OnDelete(DeleteBehavior.Cascade); // Delete Order, dann auch seine OrderItems
            // builder.HasOne(order=>order.DeliveryMethod).WithOne().OnDelete(DeleteBehavior.Cascade);
        }
    }
}
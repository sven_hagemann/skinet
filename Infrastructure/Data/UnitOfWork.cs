using System;
using System.Collections;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;

namespace Infrastructure.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly StoreContext _context;
        private Hashtable _repos;

        public UnitOfWork(StoreContext context)
        {
            _context = context;
        }

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public IGenericRepository<TEntity> Repository<TEntity>() where TEntity : BaseEntity
        {
            if (_repos == null) _repos = new Hashtable();

            var type = typeof(TEntity).Name; // Name der Entity

            // Repository in einer HashTable cachen, wenn noch nicht geschehen
            // Ein Repository wird nur 1x erstellt und dann immer wiederverwendet
            if (!_repos.ContainsKey(type))
            {
                var repoType = typeof(GenericRepository<>);

                // Erstellt eine Instanz des generischen Repository
                // Der generische Type wird beim Aufruf mitgegeben (TEntity)
                // Das Repository erwartet bei seiner Erstellung einen "StoreContext"
                //  Dieser wird hier als 2. Parameter übergeben (_context)
                //      "_context" wird nur 1x erstellt und mit allen geteilt => Singleton
                var repoInstance = Activator.CreateInstance(repoType.MakeGenericType(typeof(TEntity)), _context);

                _repos.Add(type, repoInstance);
            }

            return (IGenericRepository<TEntity>)_repos[type];
        }
    }
}
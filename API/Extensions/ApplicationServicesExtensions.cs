using System.Linq;
using API.Errors;
using Core.Interfaces;
using Infrastructure.Data;
using Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace API.Extensions
{
    public static class ApplicationServicesExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IProductRepository, ProductRepository>(); // Zum DI-Container hinzufügen
            services.AddScoped<IBasketRepository, BasketRepository>();
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>)); // Typeof, weil die Typen generisch sind

            // Alle Model-State-Errors abfangen und selber handeln
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = actionContext =>
                { // Eigenes InvalideModelState erstellen
                    var errors = actionContext.ModelState // Alle Errors aus allen Models holen
                        .Where(modelStateEntry => modelStateEntry.Value.Errors.Count > 0)
                        .SelectMany(modelStateEntry => modelStateEntry.Value.Errors) // alle Errors, welche sich verschachtelt, auf gleicher unteren Ebene befinden, zu einer Ebene mergen.
                        .Select(err => err.ErrorMessage).ToArray();

                    var errorResponse = new ApiValidationErrorResponse
                    { // ein ErrorResponse erstellen
                        Errors = errors
                    };

                    return new BadRequestObjectResult(errorResponse); // BadRequestObjectResult mit eigener Message erstellen und zurückgeben
                };
            });

            return services;
        }
    }
}
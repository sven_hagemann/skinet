using API.Dtos;
using AutoMapper;
using Core.Entities;
using Core.Entities.OrderAggregate;

namespace API.Helpers
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Product, ProductToReturnDto>()
                .ForMember(dto => dto.ProductBrand, option => option.MapFrom(entity => entity.ProductBrand.Name))
                .ForMember(dto => dto.ProductType, option => option.MapFrom(entity => entity.ProductType.Name))
                .ForMember(dto => dto.PictureUrl, o => o.MapFrom<ProductUrlResolver>()); // fügt der Adresse zum Bild den Root-Pfad hinzu => siehe appsettings[...].json

            CreateMap<Core.Entities.Identity.Address, AddressDto>() // Das reicht, da die Eigenschaften beider Objekte gleich sind.
                .ReverseMap(); // Mappen auch umgekehrt, also von AddressDto zu Address

            CreateMap<CustomerBasketDto, CustomerBasket>();

            CreateMap<BasketItemDto, BasketItem>();

            CreateMap<AddressDto, Core.Entities.OrderAggregate.Address>(); // Address ist hier ein anderes Objekt, als oben

            CreateMap<Order, OrderToReturnDto>()
                // "Order" und "OrderToReturnDto" unterscheiden sich in den Eigenschaften "DeliveryMethod" und "Price"
                //  "orderToReturnDto.DeliveryMethod" = string <==> "order.DeliveryMethod" = object
                //  "orderToReturnDto.ShippingPrice" = decimal (neue Variable) <==> "order.DeliveryMethod.Price" = decimal
                .ForMember(orderToRetDto => orderToRetDto.DeliveryMethod, expr => expr.MapFrom(order => order.DeliveryMethod.ShortName))
                .ForMember(orderToRetDto => orderToRetDto.ShippingPrice, expr => expr.MapFrom(order => order.DeliveryMethod.Price));

            CreateMap<OrderItem, OrderItemDto>()
                .ForMember(orderItemDto => orderItemDto.ProductId, expr => expr.MapFrom(orderItem => orderItem.ItemOrdered.ProductItemId))
                .ForMember(orderItemDto => orderItemDto.ProductName, expr => expr.MapFrom(orderItem => orderItem.ItemOrdered.ProductName))
                .ForMember(orderItemDto => orderItemDto.PictureUrl, expr => expr.MapFrom(orderItem => orderItem.ItemOrdered.PictureUrl))
                .ForMember(orderItemDto => orderItemDto.PictureUrl, expr => expr.MapFrom<OrderItemUrlResolver>());
        }
    }
}
namespace Core.Entities.OrderAggregate
{
    // Diese Adresse wird zur Order gemappt, sprich sie ist Bestandteil der Order-Tablle - als zusätzliche Spalten (ShipToAddress_FirstName, etc.)
    public class Address
    {
        public Address() // EF erwartet einen Parameterlosen Konstuktor, sonst bekommen wir Errors bei der Migration
        {
        }

        public Address(string firstName, string lastName, string street, string city, string state, string zipCode)
        {
            FirstName = firstName;
            LastName = lastName;
            Street = street;
            City = city;
            State = state;
            ZipCode = zipCode;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
    }
}
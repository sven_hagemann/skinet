namespace Core.Entities.OrderAggregate
{
    public class DeliveryMethod : BaseEntity // Stammdaten z.B. DHL, Hermes, etc.
    {
        public string ShortName { get; set; }
        public string DeliveryTime { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
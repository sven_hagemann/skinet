using System.Runtime.Serialization;

namespace Core.Entities.OrderAggregate
{
    public enum OrderStatus
    {
        [EnumMember(Value = "Pending")] // Die Annontation sorgt dafür, dass nicht 1,2, etc. sondern die Namen zurückgegeben werden
        Pending,
        
        [EnumMember(Value = "Payment Recived")]
        PaymentRecived,

        [EnumMember(Value = "Payment Failed")]
        PaymentFailed,

        // Shipped // Hier können noch weitere Statusse implementiert werden z.B. Shipped etc.
    }
}
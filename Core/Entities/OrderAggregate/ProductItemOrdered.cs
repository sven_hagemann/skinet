namespace Core.Entities.OrderAggregate
{
    // Ein Snapshot des bestellten Artiles => Falls sich z.B. der Name oder das Bild geändert hat
    // So kann der Artikel in der DB gespeichert werden wie er vom Client erstellt wurde, ohne ihn einer anderen Entity zuzuweisen
    // Es gibt keine Id, da es die OrderItem-Entity erweitert 
    public class ProductItemOrdered
    {
        public ProductItemOrdered() // EF erwartet einen Parameterlosen Konstuktor, sonst bekommen wir Errors bei der Migration
        {
        }

        public ProductItemOrdered(int productItemId, string productName, string pictureUrl)
        {
            ProductItemId = productItemId;
            ProductName = productName;
            PictureUrl = pictureUrl;
        }

        public int ProductItemId { get; set; }
        public string ProductName { get; set; }
        public string PictureUrl { get; set; }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;
using Core.Specifications;

namespace Core.Interfaces
{
public interface IGenericRepository<T> where T : BaseEntity
    {
        Task<T> GetByIdAsync(int id);
        Task<IReadOnlyList<T>> ListAllAsync();
        Task<T> GetEntityWithSpec(ISpecification<T> spec);
        Task<IReadOnlyList<T>> ListAsync(ISpecification<T> spec);
        Task<int> CountAsync(ISpecification<T> spec);

        // Folgende Methoden werden nicht asyncron ausgeführt, weil sie nicht gegen die DB ausgeführt werden (dafür ist UoW zuständig)
        // Die Entities werden hier getrackt (in Memory, not in DB)
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}